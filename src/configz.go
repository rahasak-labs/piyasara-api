package main

import (
	"os"
)

type Config struct {
	serviceName string
	serviceMode string
	servicePort string
	dotKeys     string
	idRsa       string
	idRsaPub    string
	dotLogs     string
}

var config = Config{
	serviceName: getEnv("SERVICE_NAME", "piyasaraapi"),
	serviceMode: getEnv("SERVICE_MODE", "DEV"),
	servicePort: getEnv("SERVICE_PORT", "8761"),
	dotKeys:     getEnv("DOT_KEYS", ".keys"),
	idRsa:       getEnv("ID_RSA", ".keys/id_rsa"),
	idRsaPub:    getEnv("ID_RSA_PUB", ".keys/id_rsa.pub"),
	dotLogs:     getEnv("DOT_LOGS", ".logs"),
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return fallback
}
