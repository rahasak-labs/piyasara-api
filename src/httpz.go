package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type Flight struct {
	Id           string `json:"id"`
	FlightNo     string `json:"flightNo"`
	FlightStatus string `json:"flightStatus"`
	FlightFrom   string `json:"flightFrom"`
	FlightTo     string `json:"flightTo"`
}

type Reply struct {
	Id      string `json:"id"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func initHttpz() {
	// router
	r := mux.NewRouter()
	r.HandleFunc("/flights", createFlight).Methods("POST")

	log.Printf("INFO: http listening on %s", config.servicePort)

	// start server
	err := http.ListenAndServe(":"+config.servicePort, r)
	if err != nil {
		log.Printf("ERROR: fail init http server, %s", err.Error)
		os.Exit(1)
	}
}

func createFlight(w http.ResponseWriter, r *http.Request) {
	// read body
	b, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	log.Printf("INFO: create flight request %s", string(b))

	// unmarshel json
	// obtain flight
	var flight Flight
	err := json.Unmarshal(b, &flight)
	if err != nil {
		log.Printf("ERROR: invalid json to unmarshal, %s", err.Error())
		return
	}

	// popup unique id
	uid := uid()
	flight.Id = uid

	// publish message to kafka

	// send response back to client
	reply := Reply{
		Id:      flight.Id,
		Code:    201,
		Message: "created",
	}
	response(w, reply, 201)
}

func response(w http.ResponseWriter, reply Reply, statusCode int) {
	j, _ := json.Marshal(reply)
	w.WriteHeader(statusCode)
	w.Header().Set("Content-Type", "application/json")

	log.Printf("write response: %s", string(j))
	io.WriteString(w, string(j))
}
