package main

import (
	"strconv"
	"time"
)

func uid() string {
	t := time.Now().UnixNano() / int64(time.Millisecond)
	return config.serviceName + strconv.FormatInt(t, 10)
}

func timestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}
