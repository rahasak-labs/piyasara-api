FROM golang:1.9

MAINTAINER Eranga Bandara (erangaeb@gmail.com)

# install dependencies
RUN	go get github.com/gorilla/mux
RUN go get github.com/gorilla/handlers
RUN go get github.com/fln/nf9packet

# env
ENV SERVICE_NAME piyasaraapi
ENV SERVICE_MODE DEV
ENV SERVICE_PORT 8761

# copy app
ADD . /app
WORKDIR /app

# build
RUN go build -o build/piyasara src/*.go

# server running port
EXPOSE 8761

# .logs volume
VOLUME ["/app/.logs"]

ENTRYPOINT ["/app/docker-entrypoint.sh"]
